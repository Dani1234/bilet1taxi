package com.example.luiza_ceciliamazare.bilet1taxi;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by luiza-cecilia.mazare on 1/20/2018.
 */

public class Cursa implements Serializable, Comparable<Cursa> {
    private String destinatie;
    private Date data;
    private TipTaxi tipTaxi;
    private float cost;

    public Cursa() {
    }

    public Cursa(String destinatie, Date data, TipTaxi tipTaxi, float cost) {
        this.destinatie = destinatie;
        this.data = data;
        this.tipTaxi = tipTaxi;
        this.cost = cost;
    }

    public String getDestinatie() {
        return destinatie;
    }

    public void setDestinatie(String destinatie) {
        this.destinatie = destinatie;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public TipTaxi getTipTaxi() {
        return tipTaxi;
    }

    public void setTipTaxi(TipTaxi tipTaxi) {
        this.tipTaxi = tipTaxi;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Cursa{" +
                "destinatie='" + destinatie + '\'' +
                ", data=" + data +
                ", tipTaxi=" + tipTaxi +
                ", cost=" + cost +
                '}';
    }

    @Override
    public int compareTo(Cursa cursa) {
        return getData().compareTo(cursa.getData());
    }
}
