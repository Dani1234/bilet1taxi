package com.example.luiza_ceciliamazare.bilet1taxi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by luiza-cecilia.mazare on 1/20/2018.
 */

public class CursaDB {
    public static final String DB_NAME="CURSA_TAXI.DB";
    public static final int DB_VERSION=1;
    public static final String CURSA_TABLE="CURSE";
    public static final String CURSA_ID="_ID";
    public static final String DESTINATIE="DESTINATIE";
    public static final String DATA="DATA";
    public static final String TIP_TAXI="TAXI";
    public static final String COST="COST";
    public static final String CREATE_TABLE_SCRIPT="CREATE TABLE "+CURSA_TABLE+" ("+CURSA_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+DESTINATIE+
            " TEXT, "+DATA+" TEXT, "+TIP_TAXI+" TEXT, "+COST+" TEXT) ";

    private DBUtility dbUtility;
    private SQLiteDatabase sqLiteDatabase;
    private Context context;

    public CursaDB(Context context){
        this.context=context;
        dbUtility=new DBUtility(context);
    }

    public void openDB(){
        sqLiteDatabase=dbUtility.getWritableDatabase();
    }

    public void closeDB(){
        sqLiteDatabase.close();
    }

    public void insertCursa(Cursa cursa){
        ContentValues contentValues=new ContentValues();
        contentValues.put(DESTINATIE, cursa.getDestinatie());
        contentValues.put(DATA, String.valueOf(cursa.getData()));
        contentValues.put(TIP_TAXI, cursa.getTipTaxi().toString());
        contentValues.put(COST, cursa.getCost());
        sqLiteDatabase.insert(CURSA_TABLE, null, contentValues);
    }

    public List<Cursa> getCurse(){
        Cursor cursor=sqLiteDatabase.query(CURSA_TABLE, null, null, null, null, null, null, null);
        List<Cursa> lst_cursa=new ArrayList<>();
        if(cursor.getCount()<1){
            cursor.close();
            return null;
        }
        if(cursor.moveToFirst()) {
            do {
                Cursa cursa = new Cursa();
                cursa.setDestinatie(cursor.getString(cursor.getColumnIndex(DESTINATIE)));
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy", Locale.ENGLISH);
                    cursa.setData(dateFormat.parse(cursor.getString(cursor.getColumnIndex(DATA))));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (cursor.getString(cursor.getColumnIndex(TIP_TAXI)).equals("normal")) {
                    cursa.setTipTaxi(TipTaxi.normal);
                } else {
                    cursa.setTipTaxi(TipTaxi.premium);
                }
                cursa.setCost(cursor.getFloat(cursor.getColumnIndex(COST)));
                lst_cursa.add(cursa);
            } while (cursor.moveToNext());
        }
            return lst_cursa;
        }

    class DBUtility extends SQLiteOpenHelper{
        public  DBUtility(Context context){
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_SCRIPT);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
