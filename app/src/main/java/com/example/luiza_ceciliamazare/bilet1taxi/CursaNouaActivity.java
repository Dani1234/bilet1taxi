package com.example.luiza_ceciliamazare.bilet1taxi;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.luiza_ceciliamazare.bilet1taxi.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CursaNouaActivity extends AppCompatActivity {
    private EditText et_destinatie;
    private EditText et_data;
    private Spinner s_tipTaxi;
    private EditText et_cost;
    private Button btn_inregistreaza;
    private Cursa cursa;
    private List<Cursa> lst_cursa;
    private TipTaxi tipTaxi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cursa_noua);

        final Intent intent=getIntent();
        Cursa cursa_salvata=(Cursa) intent.getSerializableExtra("cursa");

        lst_cursa=new ArrayList<>();
        et_destinatie=(EditText)findViewById(R.id.et_destinatie);
        et_data=(EditText)findViewById(R.id.et_data);
        s_tipTaxi=(Spinner)findViewById(R.id.s_tipTaxi);
        et_cost=(EditText)findViewById(R.id.et_cost);
        et_data.setFocusable(false);

        et_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayPicker();
            }
        });

        s_tipTaxi.setAdapter(new ArrayAdapter<TipTaxi>(this, android.R.layout.simple_spinner_dropdown_item, tipTaxi.values()));
        btn_inregistreaza=(Button)findViewById(R.id.btn_inregistreaza);
        btn_inregistreaza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int flag=0;
                cursa=new Cursa();
                if(et_destinatie.getText().toString().length()==0){
                    flag=1;
                } else {
                    cursa.setDestinatie(et_destinatie.getText().toString());
                }

                if(et_data.getText().toString().trim().length()==0){
                    flag=1;
                } else {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                    Date data= null;
                    try {
                        data = simpleDateFormat.parse(et_data.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    cursa.setData(data);
                }

                if(s_tipTaxi.getSelectedItem().toString().equals("normal")){
                    cursa.setTipTaxi(TipTaxi.normal);
                } else {
                    cursa.setTipTaxi(TipTaxi.premium);
                }

                if(et_cost.getText().toString().length()==0){
                    flag=1;
                } else {
                    cursa.setCost(Float.valueOf(et_cost.getText().toString()));
                }
                if(flag==0){
                    Intent intent1=new Intent(CursaNouaActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    CursaDB cursaDB=new CursaDB(CursaNouaActivity.this);
                    cursaDB.openDB();
                    cursaDB.insertCursa(cursa);
                    cursaDB.closeDB();
                    setResult(RESULT_OK, intent1);
                    finish();
                } else {
                    Toast.makeText(CursaNouaActivity.this, "Completati toate campurile", Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(cursa_salvata!=null){
            et_destinatie.setText(cursa_salvata.getDestinatie());
            et_destinatie.setFocusable(false);
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            et_data.setText(simpleDateFormat.format(cursa_salvata.getData()));
            if(TipTaxi.premium==cursa_salvata.getTipTaxi()){
                s_tipTaxi.setSelection(1);
            } else {
                s_tipTaxi.setSelection(0);
            }
            et_cost.setText(String.valueOf(cursa_salvata.getCost()));
            et_cost.setFocusable(false);

            s_tipTaxi.setEnabled(false);
            btn_inregistreaza.setEnabled(false);
        }
    }

    private void displayPicker(){
        Calendar calendar=Calendar.getInstance(Locale.getDefault());
        int year=calendar.get(Calendar.YEAR);
        int month=calendar.get(Calendar.MONTH);
        int day=calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog=new DatePickerDialog(CursaNouaActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dateString=dayOfMonth+"/"+(month+1)+"/"+year;
                et_data.setText(dateString);
            }
        }, year, month, day);
        datePickerDialog.show();
    }
}
