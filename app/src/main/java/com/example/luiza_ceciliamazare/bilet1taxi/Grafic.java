package com.example.luiza_ceciliamazare.bilet1taxi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import java.util.List;

public class Grafic extends AppCompatActivity {
    private ImageView graph;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_grafic);
            graph = (ImageView) findViewById(R.id.graph_img);
            graph.requestLayout();
            Intent intent = getIntent();
            List<Cursa> lst_cursa = (List<Cursa>) intent.getSerializableExtra("lista_cursa");
            int max_price = 0;
            int max_cursa = 0;
            Log.i("list size", lst_cursa.size() + "");
            for (Cursa cursa : lst_cursa) {
                max_cursa++;
                if (max_price < cursa.getCost()) {
                    max_price = (int) cursa.getCost();
                }
            }
            Paint paint_white = new Paint(Paint.LINEAR_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
            paint_white.setAntiAlias(true);
            paint_white.setColor(Color.WHITE);
            paint_white.setStyle(Paint.Style.STROKE);
            paint_white.setTextSize(30);
            Paint paint_black = new Paint();
            paint_black.setColor(Color.BLACK);
            paint_black.setStyle(Paint.Style.FILL);
            Path path = new Path();
            Bitmap btm = Bitmap.createBitmap(1000, 1500, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(btm);
            canvas.drawRGB(0, 0, 0);
            path.moveTo(50, 50);
            canvas.drawText("lei", 25, 25, paint_white);
            path.lineTo(50, btm.getHeight() - 100);
            path.lineTo(btm.getWidth() - 100, btm.getHeight() - 100);
            canvas.drawText("curse", btm.getWidth() - 90, btm.getHeight() - 90, paint_white);
            path.moveTo(50, btm.getHeight() - 100);
            int nr_curse = 0;
            for (Cursa cursa : lst_cursa) {
                nr_curse++;
                path.lineTo(((btm.getWidth() - 100) / max_cursa) * nr_curse, (btm.getHeight() - 100) - cursa.getCost());
                canvas.drawText(cursa.getCost() + "", ((btm.getWidth() - 100) / max_cursa) * nr_curse, (btm.getHeight() - 100) - cursa.getCost() - 50, paint_white);
            }
            canvas.drawPath(path, paint_white);
            graph.setImageBitmap(btm);
        }
}

