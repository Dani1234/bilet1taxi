package com.example.luiza_ceciliamazare.bilet1taxi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.example.luiza_ceciliamazare.bilet1taxi.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IstoricCursaActivity extends AppCompatActivity {
    private ListView lv_istoric;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_istoric_cursa);

        Intent intent=getIntent();
        final List<Cursa> lst_cursa=(ArrayList<Cursa>) intent.getSerializableExtra("list_cursa");
        lv_istoric=(ListView)findViewById(R.id.lv_istoric);
        Collections.sort(lst_cursa, Collections.<Cursa>reverseOrder());
        List<String> main_data=new ArrayList<>();
        for(Cursa cursa:lst_cursa){
            SimpleDateFormat simpleDateFormat=new SimpleDateFormat("dd/MM/yyyy");
            String main = simpleDateFormat.format(cursa.getData())+" - Destinatie: "+cursa.getDestinatie()+" - Tip taxi: "+cursa.getTipTaxi()
            +" - Cost: "+cursa.getCost();
            main_data.add(main);
        }

        ListAdapter arrayAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, main_data);
        lv_istoric.setAdapter(arrayAdapter);
        lv_istoric.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursa cursa=lst_cursa.get(position);
                Intent new_activity=new Intent(IstoricCursaActivity.this, CursaNouaActivity.class);
                new_activity.putExtra("cursa", cursa);
                startActivity(new_activity);
            }
        });
    }
}
