package com.example.luiza_ceciliamazare.bilet1taxi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by luiza-cecilia.mazare on 1/20/2018.
 */

public class JsonParser {

    public List<Cursa> jsonToCursa(String data) {
        List<Cursa> lst_cursa = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("rows");
            for (int i = 0; i < jsonArray.length(); i++) {
                Cursa cursa = new Cursa();
                JSONObject obj = jsonArray.getJSONObject(i);
                cursa.setDestinatie(obj.getString("destinatie"));
                DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy", Locale.ENGLISH);
                cursa.setData(dateFormat.parse(obj.getString("data")));
                if (obj.getString("tipTaxi") == "normal") {
                    cursa.setTipTaxi(TipTaxi.normal);
                } else {
                    cursa.setTipTaxi(TipTaxi.premium);
                }
                cursa.setCost(Float.valueOf(obj.getString("cost")));
                lst_cursa.add(cursa);
            }
            return lst_cursa;
        } catch (JSONException ex) {
            ex.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
}
