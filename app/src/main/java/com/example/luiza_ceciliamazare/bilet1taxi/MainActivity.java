package com.example.luiza_ceciliamazare.bilet1taxi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.luiza_ceciliamazare.bilet1taxi.R;

import org.w3c.dom.Text;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button btn_cursaNoua;
    private Button btn_istoricCursa;
    private Button btn_grafic;
    private Button btn_despre;
    private TextView tv_copyright;
    private List<Cursa> lst_cursa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lst_cursa=new ArrayList<>();

        CursaDB cursaDB=new CursaDB(this);
        cursaDB.openDB();
        lst_cursa=cursaDB.getCurse();
        cursaDB.closeDB();

        btn_cursaNoua=(Button) findViewById(R.id.btn_cursaNoua);
        btn_istoricCursa=(Button)findViewById(R.id.btn_istoricCursa);
        btn_grafic=(Button)findViewById(R.id.btn_grafic);
        btn_despre=(Button)findViewById(R.id.btn_despre);
        tv_copyright=(TextView) findViewById(R.id.tv_despre);

        btn_cursaNoua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, CursaNouaActivity.class);
                startActivity(intent);
            }
        });

        btn_istoricCursa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, IstoricCursaActivity.class);
                intent.putExtra("list_cursa", (Serializable) lst_cursa);
                startActivity(intent);
            }
        });

        btn_grafic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, Grafic.class);
                intent.putExtra("lista_cursa", (Serializable) lst_cursa);
                startActivity(intent);
            }
        });

        btn_despre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, tv_copyright.getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode==RESULT_OK){
            CursaDB cursaDB=new CursaDB(this);
            cursaDB.openDB();
            lst_cursa=cursaDB.getCurse();
            cursaDB.closeDB();
        }
    }
}
