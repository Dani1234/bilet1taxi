package com.example.luiza_ceciliamazare.bilet1taxi;

/**
 * Created by luiza-cecilia.mazare on 1/20/2018.
 */

public enum TipTaxi {
    normal, premium;
}
